cmake_minimum_required(VERSION 3.11)

project(chapter02_console LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

if(WIN32)
    add_definitions("/wd4819")
    add_definitions("/WX")
elseif(UNIX)
    add_definitions("-Werror")
endif()

list(APPEND CMAKE_MODULE_PATH $ENV{DEPENDENCY_PATH}/glib)

find_package(glib REQUIRED)

if(WIN32)
    option(USE_VLD "use vld" ON)
    if(USE_VLD)
        add_definitions("-DUSE_VLD")
        list(APPEND CMAKE_MODULE_PATH $ENV{DEPENDENCY_PATH}/vld)
        find_package(vld REQUIRED)
        include_directories(
            ${VLD_INCLUDE_DIR})
    endif()
endif()

include_directories(
    ${GLIB_INCLUDE_DIR})

set(PROJECT_SOURCE
    console.cc)

add_executable(
    ${PROJECT_NAME}
    ${PROJECT_SOURCE})

target_link_libraries(
    ${PROJECT_NAME}
    ${GLIB_LIBRARY})

if(WIN32)
    if(USE_VLD)
        target_link_libraries(
            ${PROJECT_NAME}
            ${VLD_LIBRARY})
    endif()
endif()
